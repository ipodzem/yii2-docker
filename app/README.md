<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h1 align="center">Biodata - Yii 2 Advanced Project Template</h1>
    <br>
</p>

Docker для YII c официального репозитория https://github.com/yiisoft/yii2-docker
WebServer Apache
Проект развернут в папке app

API: http://localhost:23081/v1/users

frontend: http://localhost:20081/

backend: http://localhost:21081/
Логин admin  пароль 12345678

Параметры facebook и токен для API 
common/params.php
