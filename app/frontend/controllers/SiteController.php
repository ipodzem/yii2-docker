<?php
namespace frontend\controllers;

use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use common\models\LoginForm;
use common\models\BiodataUser;
use common\models\Bonus;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    private $biodataUser;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'profile'],
                'rules' => [

                    [
                        'actions' => ['logout', 'profile', 'bonus'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'oAuthSuccess'],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if(in_array($action->id, ['profile', 'bonus'])) {
            $data = \Yii::$app->session->get('user_info');
            if(empty($data))
                throw(new BadRequestHttpException);
            $this->biodataUser = biodataUser::findBySource($data['source'], $data['source_id']);
        }
        return parent::beforeAction($action); // TODO: Change the autogenerated stub
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionProfile()
    {
        return $this->render('profile', [
            'data' => \Yii::$app->session->get('user_info'),
            'biodataUser' => $this->biodataUser
        ]);
    }

    public function actionBonus() {

        $bonus = Bonus::getAvailableBonus();
        $this->biodataUser->setBonus($bonus);
        Yii::$app->response->format = yii\web\Response::FORMAT_JSON;
        return compact('bonus');
    }

    public function oAuthSuccess($client) {
        $userAttributes = $client->getUserAttributes();

        if($client instanceof yii\authclient\clients\Facebook) {
            $biodataUser = BiodataUser::findBySource(biodataUser::SOURCE_FACEBOOK, $userAttributes['id']);
            if (!$biodataUser)
                $biodataUser = BiodataUser::create(biodataUser::SOURCE_FACEBOOK, $userAttributes['id']);
            $user_info = [];
            $user_info['name'] = $userAttributes['name'];
            $user_info['email'] = $userAttributes['email'];
            $user_info['logo'] = $userAttributes['picture']['data']['url'];
            $user_info['source'] = biodataUser::SOURCE_FACEBOOK;
            $user_info['source_id'] = $userAttributes['id'];
            Yii::$app->session->set('user_info', $user_info);
            Yii::$app->user->login($biodataUser->user);
            return;
        }
        throw(new BadRequestHttpException);

    }
}
