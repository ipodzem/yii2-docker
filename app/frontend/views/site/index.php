<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;
$this->title = 'Biodata Profile';
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <?php if (Yii::$app->user->isGuest)  {
                echo 'Пожалуйста, авторизуйтесь, используя кнопку в правом верхнем углу';
            } else {
                echo 'Добро пожаловать! Вы можете перейти в свой '.Html::a('профиль', ['site/profile']);
            }?>
        </div>

    </div>
</div>
