<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->registerJsFile('@web/js/profile.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->title = 'Профиль';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="row">
        <div class="col-lg-4">
            <p>
                <?php if(isset($data['logo'])):?>
                    <?= Html::img($data['logo'], ['alt' => 'My logo']) ?>
                <?php endif;?>
            </p>
        </div>
        <div class="col-lg-8">
            <p>
                <?= $data['name']?>
            </p>
            <p>
                <?= $data['email']?>
            </p>
        </div>
    </div>
<?php if($biodataUser->bonus_id == 0):?>
    <div class="row">
        <?= Html::button('Получить бонус', ['class' => 'get_bonus', 'data-url' => Url::to(['site/bonus'])]) ?>
    </div>
<?php endif;?>