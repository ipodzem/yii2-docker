<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;
/**
 * User model
 *
 * @property integer $id
 * @property string $name
 * @property integer $count
 */
class Bonus extends ActiveRecord
{


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%bonus}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['name', 'string'],
            ['count', 'integer'],
        ];
    }

    public function scenarios()
    {
        return [
            'default' => ['name', 'count'],
            'change_cnt' => ['count']
        ];
    }

    public static function getAvailableBonus()
    {
        $bonuses = self::getActiveList();
        shuffle($bonuses);
        $bonus = [];
        do {
            $tmp = array_pop($bonuses);
            if($tmp->count > 0 || $tmp->count = '-1')
                $bonus = $tmp;
        } while (empty($bonus));
        return $bonus;
    }

    public static function getIds()
    {
        return static::find()->select("id")->asArray()->column();
    }

    public static function getActiveList()
    {
        return static::find()->where(['<>', 'count', '0'])->all();;
    }

    public  function DecreateCnt()
    {
        if($this->count > 0) {
            $this->scenario = 'change_cnt';
            $this->count = $this->count - 1;
            return $this->save();
        }
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

}
