<?php
namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\data\ActiveDataProvider;
/**
 * User model
 *
 * @property integer $id
 * @property string $source_id
 * @property string $source
 * @property integer $bonus_id
 */
class BiodataUser extends ActiveRecord
{
    CONST SOURCE_FACEBOOK = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%biodata_user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['source_id', 'source'], 'required'],
            ['source_id', 'string', 'max' => 255],
            ['bonus_id', 'integer'],
            ['bonus_id', 'in', 'range' => array_merge([0], Bonus::getIds())],
            ['source', 'in', 'range' => [self::SOURCE_FACEBOOK]],
        ];
    }
    public function fields() {
        return [
            'id',
            'created_at' => function($model) {
                return date("d.m.Y", $model->user->created_at);
            },
            'bonus' => function($model) {
                if(isset($model->bonus))
                    return $model->bonus->name;
                else
                    return '-';
            }
        ];
    }

    public static function create($source, $source_id)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $user = new User;
            $user->status = User::STATUS_ACTIVE;
            $user->username = 'SocialUser_' . $source_id;
            $user->generateAuthKey();
            $user->setPassword($user->auth_key);
            if ($user->save()) {
                $biodataUser = new BiodataUser;
                $biodataUser->user_id   = $user->id;
                $biodataUser->source    = $source;
                $biodataUser->source_id = $source_id;
                $biodataUser->bonus_id  = 0;
                if ($biodataUser->save()) {
                    $transaction->commit();
                    return $biodataUser;
                }


            }


        } catch (Exception $ex) {
            $transaction->rollback();
            return false;
        }
    }


    /**
     * {@inheritdoc}
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id'])->andWhere(['status' => User::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBonus()
    {
        return $this->hasOne(Bonus::className(), ['id' => 'bonus_id']);
    }

    public function setBonus($bonus)
    {
        $this->bonus_id = $bonus->id;
        $this->save();
        $bonus->decreateCnt();

    }

    /**
     * {@inheritdoc}
     */
    public static function findBySource($source, $source_id)
    {
        return static::findOne(['source_id' => $source_id, 'source' => $source]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

}
