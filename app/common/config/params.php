<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,
    'user.passwordMinLength' => 8,
    'apiToken' => 'superToken237843',
    'facebook_secret' => '2a362c8877baa9d7b90feaee268a3707',
    'facebook_id' => '500917637926377'
];
