<?php

namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
/**
 * User Controller API
 *
 */
class UserController extends ActiveController
{
    public $modelClass = 'common\models\BiodataUser';


    public function actions()       // Just read only rest api
    {
        $actions = parent::actions();
        unset($actions['delete'], $actions['create'], $actions['update']);
        return $actions;
    }

    public function behaviors() {

        $behaviors = parent::behaviors();

        $behaviors['access'] =   [
            'class' => AccessControl::className(),
            'user' => false,
            'rules' => [
                [
                    'allow' => true,
                    'matchCallback' => function ($rule, $action) {
                        $data = \Yii::$app->getRequest()->getHeaders();
                        return isset($data['token']) && $data['token']=== \Yii::$app->params['apiToken'];
                    },
                ],
            ],
            'denyCallback' => function ($rule, $action) {
                $data = \Yii::$app->getRequest()->getHeaders();

                throw new ForbiddenHttpException(\Yii::t('app', 'Доступ запрещен'));
            }
        ];
        return $behaviors;
    }



}


