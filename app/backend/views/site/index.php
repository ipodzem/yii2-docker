<?php
use yii\grid\GridView;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Спиcок пользователей</h1>
    </div>

    <div class="body-content">

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                ['attribute' => 'user.created_at', 'format' => ['date', 'dd.MM.YYYY'], 'header' => 'Дата регистрации'],
                ['attribute' => 'bonus.name', 'header' => 'Бонус']


            ],
        ]); ?>

    </div>
</div>
