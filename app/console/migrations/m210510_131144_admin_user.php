<?php

use yii\db\Migration;

/**
 * Class m210510_131144_admin_user
 */
class m210510_131144_admin_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $time = time();
        $password_hash = Yii::$app->getSecurity()->generatePasswordHash('12345678');
        $auth_key = Yii::$app->security->generateRandomString();

        $this->insert('{{%user}}', [
            'username'      => 'admin',
            'email'         => 'ipodzemelnaya@gmail.com',
            'password_hash' => $password_hash,
            'auth_key'      => $auth_key,
            'created_at'    => $time,
            'updated_at'    => $time
            ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%user}}', ['username' => 'admin']);

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210510_131144_admin_user cannot be reverted.\n";

        return false;
    }
    */
}
