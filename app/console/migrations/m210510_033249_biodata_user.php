<?php

use yii\db\Migration;

/**
 * Class m210510_033249_biodata_user
 */
class m210510_033249_biodata_user extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%biodata_user}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'source_id' => $this->string()->notNull()->unique(),
            'source' => $this->integer()->notNull(),
            'bonus_id' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->addForeignKey('fk-biodata-user-user_id-user-id', 'biodata_user', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
        $this->dropTable('{{%biodata_user}}');
    }
}
