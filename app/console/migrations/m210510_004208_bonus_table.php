<?php

use yii\db\Migration;

/**
 * Class m210510_004208_bonus_table
 */
class m210510_004208_bonus_table extends Migration
{
     public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%bonus}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'count' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->insert('{{%bonus}}', [
            'name' => 'Бесплатное обследование',
            'count' => '10',
        ]);
        $this->insert('{{%bonus}}', [
            'name' => 'Скидка на поездку в санаторий ',
            'count' => '-1',
        ]);

        $this->insert('{{%bonus}}', [
            'name' => 'Кружка с логотипом “БиоДата”',
            'count' => '20',
        ]);

}

    public function down()
    {
        $this->delete('{{%bonus}}', ['id' => 1, 'id' => 2,'id' => 3]);
        $this->dropTable('{{%bonus}}');
    }
}
