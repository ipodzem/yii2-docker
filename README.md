Переходим в папку app, выполняем команду docker-compose up -d

docker exec -it app_backend_1 composer install

docker exec -it app_backend_1 php /app/init Development


Меняем Настройки db в файле
 
common\config\main-local.php

'db' => [

 'class' => 'yii\db\Connection',

 'dsn' => 'mysql:host=app_mysql_1;dbname=biodata',

 'username' => 'biodata',

 'password' => 'secret',

 'charset' => 'utf8',

],

Настройки facebook, token для API в файле
common/config/params.php
